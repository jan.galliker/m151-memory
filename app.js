let express = require('express');
let session = require('express-session');
let multer = require('multer');
let app = express();
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let MongoStore = require('connect-mongo');
let cookieParser = require('cookie-parser');
let bcrypt = require('bcrypt');

//object with predefined cookiesettings for later use
const cookieSettings = {maxAge: 60000, httpOnly: true, sameSite: 'lax'}

require('dotenv/config');
let helper = require('./helper');

//preparing multer for use
let storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, 'static')
	},
	filename: (req, file, cb) => {
		cb(null,`${Date.now()}-${Math.floor(Math.random()*10e8)}-${file.originalname}`)
	}
});
let upload = multer({storage: storage});

//making a static fileserver
app.use('/static',express.static(__dirname+'/static'));

//defining some functionality
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json(),cookieParser());

//connect to the mongoDb defined in the .env file
mongoose.connect(process.env.MONGODB_URL, err => {
    if (err)
    {
        console.log(err);
    }
    else
    {
        console.log("Connect to " + process.env.MONGODB_URL);
    }
});

//preparing session for usage
app.use(session({
    secret: bcrypt.genSaltSync(10),
    resave: false,
    rolling: true,
    saveUninitialized: false,
    store: MongoStore.create(
        mongoose.connection
    ),
    cookie:
    {
        maxAge: Date.now()+1800000,
        httpOnly: true,
        sameSite: 'lax'
    }
}));

//adding the models into app.js
let user = require('./models/userModel');
let memory = require('./models/memoryModel');

//setting the view engine to ejs
app.set("view engine","ejs");

//redirect to the actual main page
app.get('/', (req,res) => {
    console.log(`GET request to '/' from ${req.session.userId ?? req.socket.remoteAddress}`);
    res.redirect('/memory');
});

//renders a error page, if something would go wrong, the user will be sent to this handler
app.get('/error', (req,res) => {
    console.log(`GET request to '/error' from ${req.session.userId ?? req.socket.remoteAddress}`);
    res.render("error")});

//memory overview Page, will show all memories in the storage
app.get('/memory', isLoggedIn , async (req,res) => {
    console.log(`GET request to '/memory' from ${req.soession.userId}`);

    let searchQuery;
    let user;

    //depending on the value that was given into the get request the memory displayed will be filtered
    switch(req.query.query)
    {
        case 'myMemories':
            user = await user.findById(req.session.userId);
            console.log(user);
            searchQuery = {username: user.username};
            break;
        case 'favorites':
            user = await user.findById(req.session.userId);
            console.log(user);
            searchQuery = {_id : {$in: user.favoriteMemories}}
            break;
        case 'completed':
            user = await user.findById(req.session.userId);
            console.log(user);
            searchQuery = {_id : {$in: user.completedMemories}};
            console.log(searchQuery);
            break;
        case 'unplayed':
            user = await user.findById(req.session.userId);
            console.log(user);
            searchQuery = {_id : {$nin: user.completedMemories}};
            console.log(searchQuery);
            break;
        default:
            searchQuery = {};
            break;
    }

    memory.find(searchQuery, (err,memories) => {
        if(err)
        {
            console.log(err);
            res.redirect("error");
        }
        else
        {
            res.render("memory",{memories: memories,user: req.session.userId, isAdmin: req.session.isAdmin});
        }
    })
});

//this will mark a memory you selected as a favorite of yours
app.post('/memory/favorite', isLoggedIn, (req,res) => {
    console.log(`POST request to '/memory/favorite' from ${req.session.userId}`);
    let memoryId = req.body.id

    //checks if there has been an memoryid provided
    if(!memoryId)
    {
        res.redirect('/memory');
    }

    //adds memory to the users favorite list
    user.findByIdAndUpdate(req.session.userId,{$push: {favoriteMemories: memoryId}}, (err,item) => {
        if(err)
        {
            console.log(err);
            res.redirect('/error');
        }
        else
        {
            //adds user to the memorys favored by list
            memory.findByIdAndUpdate(memoryId,{$push: {favoredBy: req.session.userId}}, (err,item) => {
                if(err)
                {
                    console.log(err);
                    res.redirect('/error');
                }
                else
                {
                    res.redirect('/memory');
                }
            });
        }
    });
});

//page for uploading a new memory
app.get('/memory/create', isLoggedIn, (req,res) => {
    console.log(`GET request to '/memory/create' from ${req.session.userId}`);

    res.render('memoryCreation', {isAdmin: req.session.isAdmin});
});

//logic to upload a new memory
app.post('/memory/create', isLoggedIn , upload.array('memoryImage',20),async (req,res) => {
    console.log(`POST request to '/memory/create' from ${req.session.userId}`);

    //checks for if some requirements are met: 

    if(req.files.length < 5 || req.files.length > 20)
    {
        res.redirect('/memory/create');
        return;
    }

    if(!req.body.memoryTitle || !req.body.memoryDescription)
    {
        res.redirect('/memory/create');
        return;
    }

    let arr=[];

    //foreach file uploaded, create a new tile object and add it to a memory
    for (let file of req.files)
    {
        let newTile = {
                title: file.originalname,
                type: file.mimetype,
                imagePath: file.filename,
            }
        arr.push(newTile);
    };

    //find the user who created memory
    let user = (await user.findById(req.session.userId)) ?? "Unknown User"

    //create new memory object and push it to the database

    let newMemory =
    {
        title: req.body.memoryTitle,
        description: req.body.memoryDescription,
        createdAt: new Date(),
        username: user.username,
        tiles: arr
    };

    memory.create(newMemory, (err,item) => {
        if(err)
        {
            console.log(err);
            res.redirect("error");
        }
        else
        {
            console.log("New Memory Created");
            res.redirect('/memory');
        }
    });
})

//initalization of the memory for playing
app.get('/memory/play', isLoggedIn, async (req,res) => {
    console.log(`GET request to '/memory/play' from ${req.session.userId}`);

    //find memory and check for its existence

    let memory = await memory.findById(req.query.id);

    if(!memory)
    {
        redirect('memory');
        return;
    }

    //getting tiles and shuffling them
    let tiles = memory.tiles;
    let sessionTiles = JSON.parse(JSON.stringify(tiles)).concat(JSON.parse(JSON.stringify(tiles)));
    helper.shuffle(sessionTiles);

    //adding values to session for usage during the playing of memory.
    //Because important information is stored in here, it is much harder for the user to cheat
    req.session.memoryTiles = sessionTiles;
    req.session.memoryId = memory.id;
    req.session.pairsUncovered = 0;
    req.session.cardsrevealed = 0;
    req.session.openedTiles = [];

    res.render('play',{numberTiles: sessionTiles.length, isAdmin: req.session.isAdmin});
});

//logic handler for during playing of memory
app.get('/memory/playing', isLoggedIn, async (req,res) => {
    console.log(`GET request to '/memory/playing' from ${req.session.userId}`);

    //getting some informations about the current game and validates some of them
    let id = req.query.id;
    let image = req.session.memoryTiles[id];

    if(!image || !image.title)
    {
        res.statusMessage = "Invalid Id";
        res.status(400).end();
        return;
    }

    req.session.cardsrevealed;
    let openedTiles = req.session.openedTiles;

    //checks if the tile should already be revealed
    if(openedTiles[0] && id == openedTiles[0].id || openedTiles[1] && id == openedTiles[1].id)
    {
        res.statusMessage = "Card with Id already revealed";
        res.status(400).end();
        return;
    }

    let resObject = {};
    req.session.cardsrevealed++;


    //main logic for memory
    if(req.session.cardsrevealed == 1)
    {
        if (openedTiles.length == 2)
        {
            console.log("Length of Opened Tiles is 2");
            if(openedTiles[0] && openedTiles[1] && openedTiles[0].path == openedTiles[1].path)
            {
                //if the revealed cards are the same, they will get removed
                resObject[openedTiles[0].id] = {path: "empty.png", desc: "blank"};
                req.session.memoryTiles[openedTiles[0].id] = {};
                resObject[openedTiles[1].id] = {path: "empty.png", desc: "blank"};
                req.session.memoryTiles[openedTiles[1].id] = {};
            }
            else
            {
                //if the revealed cards are different, the will get turned back over
                resObject[openedTiles[0].id] = {path: "backgroundtile.png", desc: "hidden"};
                resObject[openedTiles[1].id] = {path: "backgroundtile.png", desc: "hidden"};        
            }
            while(openedTiles.length > 0) {
                openedTiles.pop();
            }
        }
        resObject[id] = {path: image.imagePath, desc: image.title};
        openedTiles.push({path: image.imagePath, id: id, desc: image.title});
    }
    else
    {
        resObject[id] = {path: image.imagePath, desc: image.title};
        if (openedTiles[0] && image.imagePath == openedTiles[0].path)
        {
            let uncoveredPairs = ++req.session.pairsUncovered;

            if(uncoveredPairs == req.session.memoryTiles.length / 2)
            {
                //if all pairs are uncovered, this part will be called, it reset all variables for playing memory
                await user.findByIdAndUpdate(req.session.userId, {$push: {completedMemories: req.session.memoryId}});
                await memory.findByIdAndUpdate(req.session.memoryId, {$push: {completedBy: req.session.userId}});

                req.session.memoryTiles = null;
                req.session.pairsUncovered = 0;
                req.session.cardsrevealed = 0;
                req.session.openedTiles = [];
                req.session.memoryId = "";

                res.json({winMessage: "You have won"});
                return;
            }
        }
        openedTiles.push({path: image.imagePath, id: id, desc: image.title});
        req.session.cardsrevealed = 0;
    }
    console.log(openedTiles.length);
    console.log(req.session.cardsrevealed);
    res.json({tileChanges: resObject});
});

//page for login
app.get('/login', (req,res) => {
    //if the user is already logged in, he shouldn't access this page
    if(req.session.userId)
    {
        res.redirect('/memory');
        return;
    }
    console.log(`GET request to '/login' from ${req.socket.remoteAddress}`);

    //cookies are used to transmit error information, e. g. if he used the wrong credentials
    let cookie = req.cookies["error"];
    if(cookie)
    {
        cookie = JSON.parse(cookie);
    }
    let errorMessage = cookie ? {[cookie.type]: cookie.message} : {};

    res.clearCookie("error");
    //because login and register are similar, they use the same template file. but to differentiate I use the type type
    res.render("authentificate",{type: "Login",error: errorMessage});
});

//checks if the user submitted the right credentials and creates depending on that a new session for him
app.post('/login', (req,res) => {
    console.log(`POST request to '/login' from ${req.socket.remoteAddress}`);

    //checks if the username provided exists in the database
    user.findOne({username:req.body.username}, (err,item) => {
        if(err)
        {
            console.log(err);
            res.redirect("error");
            return;
        }
        else if (item !== null)
        {
            //checks if the password provided is the same as the one in the database
            if(bcrypt.compareSync(req.body.password,item.password))
            {
                //sets session
                req.session.userId = item.id;
                req.session.isAdmin = item.isAdmin;
                console.log(`${req.socket.remoteAddress} is logged in as ${req.session.userId}`);
                
                res.redirect('/memory');
                return;
            }
        }
        //if the credentials provided are incorrect it will create a cookie and redirects the user back to the loginpage
        res.cookie('error',JSON.stringify({type: 'invalidLogin', message: 'Invalid Login'}),cookieSettings);
        res.redirect('/login');
    });
});

//page for register
app.get('/register', (req,res) => {
    //if the user is already logged in, he shouldn't access this page
    if(req.session.userId)
    {
        res.redirect('/memory');
        return;
    }

    console.log(`GET request to '/register' from ${req.socket.remoteAddress}`);

    //cookies are used to transmit error information, e. g. if he used the wrong credentials
    let cookie = req.cookies["error"];
    if(cookie)
    {
        cookie = JSON.parse(cookie);
    }
    let errorMessage = cookie ? {[cookie.type]: cookie.message} : {};

    res.clearCookie("error");
    //because login and register are similar, they use the same template file. but to differentiate I use the type type
    res.render("authentificate",{type: "Register", error: errorMessage});
});

//checks if a the username is new and if the password follows the guidelines and creates a new user if both were successful
app.post('/register', async (req,res) => {
    console.log(`POST request to '/register' from ${req.socket.remoteAddress}`);
    try
    {
        //the username should be longer than 3 characters
        if (req.body.username.length < 3)
        {
            res.cookie('error',JSON.stringify({type: 'badUsername', message: 'Username must contain at least three characters'}),cookieSettings);
            res.redirect('/register');
            return;
        }

        //the usernames should be unique or else it could create some problems. this part checks if a username already exists 

        let user = await user.findOne({username: req.body.username})
        
        if (user !== null)
        {
            res.cookie('error',JSON.stringify({type: 'badUsername', message: 'Username already taken'}),cookieSettings);
            res.redirect('/register');
            return;
        }

        //checks if the selected password meets some requirements (see helper.validate())
        let validationResult = helper.validate(req.body.password);

        if(!validationResult.isSuccess)
        {
            res.cookie('error',JSON.stringify({type: 'insufficentPassword', message: validationResult.errorMessage}),cookieSettings);
            res.redirect('/register');
            return;
        }

        //hashes password for privacy reasons
        let salt = bcrypt.genSaltSync(10)
        let hash = bcrypt.hashSync(req.body.password,salt,10)

        //creates user object and adds it into the database
        let newUser = {
            username: req.body.username,
            password: hash,
            salt : salt,
            isAdmin: false,
            createdAt: new Date(),
            completedMemories: [],
            favoriteMemories: []
        };
        let dbUser = await user.create(newUser);

        //creates session
        req.session.userId = dbUser.id;
        console.log(`${req.socket.remoteAddress} is logged in as ${req.session.userId}`);
        
        res.redirect('/memory');      
    }
    catch (err)
    {
        console.log(err);
        res.redirect("error");
    }
});

//this handler will destroy a session of a user
app.get('/logout', (req,res) => {
    console.log(`GET request to '/logout' from ${req.socket.remoteAddress}`);

    req.session.destroy();
    res.redirect('/login');
})

//page for handling users
app.get('/admin', isAdmin, (req,res) => {
    console.log(`GET request to '/admin' from ${req.socket.remoteAddress}`);

    user.find({},(err,items) => {
        if(err)
        {
            console.log(err);
            res.redirect("error");
        }
        else
        {
            res.render('usersOverview',{users: Array.from(items)})
        }
    });
});

//a request to this handler will delete the user with the corresponing id
app.post('/admin/deleteUser', isAdmin, (req,res) => {
    console.log(`POST request to '/admin/deleteUser' from ${req.socket.remoteAddress}`);

    user.findByIdAndDelete(req.body.id, (err,item) =>{
        if(err)
        {
            console.log(err);
            res.redirect("error");;
        }
        else
        {
            res.redirect('/admin');
        }
    });
});

//this handler toggles the admin rights of a user
app.post('/admin/modifyRole', isAdmin, (req,res) => {
    console.log(`POST request to '/admin/modifiyRole' from ${req.socket.remoteAddress}`);

    user.findByIdAndUpdate(req.body.id,[{$set:{isAdmin: {$not : "$isAdmin"}}}], (err,item) => {
        if(err)
        {
            console.log(err);
            res.redirect("error");
        }
        else
        {
            res.redirect('/admin');
        }
    });
});

//handler to search for a certain user and sends the users via json back
app.get('/admin/searchUser', isAdmin, (req,res) => {
    console.log(`Get request to '/admin/searchUser' from ${req.socket.remoteAddress}`);

    //checks if name is not null or undefined
    if(!req.query.name && req.query.name !== "")
    {
        res.statusMessage = "Invalid Name";
        res.status(400).end();
        return;
    }

    let lookforName = req.query.name;

    //replaces all for regex relevant symbols
    lookforName = lookforName.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'); 

    //checks if the lookup is a part of a name and returns all names
    user.find({ username: {$regex: lookforName, $options: "i"}}, (err,items) => {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.json(items)
        }
    });
});

//set the port of the application
let port = process.env.PORT || 3000;
app.listen(port, err => {
    if(err)
    {
        throw err
    }
    console.log('Server started, listening on port '+ port);
})

//helper function to check if a user is logged in if not it will redirect him to the login page 
function isLoggedIn(req,res,next)
{
    if(req.session.userId)
    {
        next();
    }
    else
    {
        console.log(`${req.socket.remoteAddress} tried to access a webpage without the proper rights`);
        res.redirect('/login');
    }
}

//helper function to check if a user is logged in if not it will redirect him to the main page
function isAdmin(req,res,next)
{
    if(req.session.isAdmin)
    {
        next()
    }
    else
    {
        console.log(`${req.session.userId ?? req.socket.remoteAddress} tried to access an Admin resticted area without the proper rights`);
        res.redirect('/memory');
    }
}