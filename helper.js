//this function will validate a password. In case of success it will return an object = {isSuccess:true}
//otherwise it will return an object = {isSuccess: false, errorMessage: "message"}
function validatePassword(password)
{ 
    let returnObject = {isSuccess: false};
    //checks if the profilepicture is longer than eight characters
    if(password.length<8)
    {
        returnObject.errorMessage = 'Password must be at least 8 characters long';
    }
    //checks if the password contains at least one letter
    else if(password.search(/[a-z]/i)<0)
    {
        returnObject.errorMessage = 'Password contain at least one letter';
    }
    //checks if the password contains at least one number
    else if(password.search(/[0-9]/)<0)
    {
        returnObject.errorMessage = 'Password contain at least one number';
    }
    //checks if the password conains at least one special character
    else if(password.search(/[^A-Za-z0-9]/)<0)
    {
        returnObject.errorMessage = 'Password contain at least one special character';
    }
    else
    {
        returnObject.isSuccess = true;
    }
    return returnObject;
}

//function to shuffle an array
function shuffleArray(array)
{
    //it swaps two array entities array.length times 
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

module.exports = {
    shuffle: shuffleArray,
    validate: validatePassword
};