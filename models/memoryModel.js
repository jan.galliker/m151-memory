let mongoose = require('mongoose');

let tileSchema = new mongoose.Schema({
    title: String,
    type: String,
    imagePath: String
});

let memorySchema = new mongoose.Schema({
    title: String,
    description: String,
    createdAt: Date,
    username: String,
    tiles: [tileSchema],
    completedBy: [String],
    favoredBy: [String],
});

module.exports = new mongoose.model('Memory',memorySchema);