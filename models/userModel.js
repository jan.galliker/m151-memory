let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    username: String,
    password: String,
    salt: String,
    isAdmin: Boolean,
    createdAt: Date,
    completedMemories: [String],
    favoriteMemories: [String],
});

module.exports = new mongoose.model('User',userSchema);