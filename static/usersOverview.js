let form = document.getElementById("searchForm");

let timer;

form.onsubmit = function()
{
    clearTimeout(timer);
    getNames(form.name.value).then();
    return false;
}

form.name.oninput = function(event)
{
    clearTimeout(timer);
    console.log("inputfield Changed");
    timer = setTimeout(() => getNames(event.target.value),1500);
}

async function getNames(string)
{
    let users = [];
    let tbody = document.querySelector("tbody");
    try
    {
        let request = await fetch(`./admin/searchUser?name=${string}`);
        users = await request.json();
    }
    catch
    {}
    tbody.innerHTML = ""
    for(let user of users)
    {
        let row = createRow(user);
        tbody.insertAdjacentElement("beforeend",row);
    }
}

function createRow(user)
{
    let tr = document.createElement('tr');

    let td = document.createElement('td');
    td.textContent = user.username;
    tr.append(td);

    td = document.createElement('td');
    td.textContent = user.createdAt;
    tr.append(td);

    td = document.createElement('td');
    td.textContent = user.isAdmin ? "Admin" : "User";
    tr.append(td);

    td = document.createElement('td');
    td.innerHTML = `<form action="/admin/deleteUser" method="POST" enctype="application/x-www-form-urlencoded">
                        <input type="hidden" name="id" value="${user.id}">
                        <button type="submit">Delete</button>
                    </form>`;
    tr.append(td);

    td = document.createElement('td');
    td.innerHTML = `<form action="/admin/modifyRole" method="POST" enctype="application/x-www-form-urlencoded">
                        <input type="hidden" name="id" value="${user.id}">
                        <button type="submit">${user.isAdmin ? "revoke Adminrights" : "give Adminrights"}</button>
                    </form>`;
    tr.append(td);

    return tr;
}