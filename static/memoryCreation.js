let counter = 6

function addField()
{
    document.getElementById("buttonRemove").disabled = false;

    if (counter>=20)
    {
        document.getElementById("buttonAdd").disabled = true;
        if(counter>20)
        {
            return;
        }
    }

    let div=document.createElement('div');

    let label=document.createElement('label');
    label.for = "memoryImage";
    label.textContent = "Tile Image ";

    let input = document.createElement('input');
    input.type = 'file';
    input.name = 'memoryImage'
    input.id = 'memoryImage'+counter;
    input.required = true;

    div.id = "div"+counter;
    div.append(label);
    div.append(input);
    document.getElementById("div"+ (counter-1)).insertAdjacentElement("afterend",div)
    counter++;
}

function removeField()
{
    document.getElementById("buttonAdd").disabled = false;

    if(counter<=7)
    {
        document.getElementById("buttonRemove").disabled = true;
        if(counter<7)
        {
            return;
        }
    }

    let div = document.getElementById("div"+ (counter-1));
    
    div.remove();

    counter--;
}