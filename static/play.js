let field = document.getElementById("field");

field.onclick = async function(event)
{
    let tile = event.target.closest('.tile');

    if(!tile)
    {
        return;
    }

    let tileNumber = tile.id;

    let response = await makeRequest(tileNumber.slice(7));
    if(!response)
    {
        return;
    }
    let imagesPath = response.tileChanges
    if(!imagesPath)
    {
        let message = response.winMessage;
        if(message)
        {
            alert(message);
            window.onbeforeunload = null;
            location.href = "./";
        }
        return;
    }

    imagesPath = new Map(Object.entries(imagesPath));

    console.log(imagesPath)

    for (let [key,value] of imagesPath)
    {
        console.log(key+","+value);

        let image = document.querySelector(`#divTile${key} img`);

        console.log(image)

        if(image)
        {
            image.src = "../static/"+value.path;
            image.alt = value.desc;
        }
    }
}

async function makeRequest(id)
{
    try
    {
        let res = await fetch(`/memory/playing?id=${id}`);
        return await res.json();
    }
    catch(err)
    {
    }
}

window.onbeforeunload = function() {
    return false;
  };